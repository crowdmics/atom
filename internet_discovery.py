import socket
import requests
import netifaces as ni

guid = socket.gethostname().split(".")[0].replace("crowdmics-", "")
internal_ip = ni.ifaddresses('eth0')[2][0]['addr']
post_data = {'guid': guid, 'internal_ip': internal_ip}

r = requests.post("http://52.89.197.201:8000/atom", json=post_data)