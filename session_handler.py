import SocketServer
import subprocess, signal, time
from threading import Thread


class MyTCPHandler(SocketServer.BaseRequestHandler):
    runningIpAddress = None

    def handle(self):
        if (MyTCPHandler.runningIpAddress != None and MyTCPHandler.runningIpAddress != self.client_address[0]):
            return

        self.data = self.request.recv(1024).strip()

        print 'Killing old process'
        subprocess.Popen(['pkill', 'tcpproxy_server'])
        time.sleep(2)

        print 'Opening process'
        subprocess.Popen(["/home/debian/atom/tcpproxy_server", "0.0.0.0", "4484", self.client_address[0], self.data])

        print "{} wrote:".format(self.client_address[0])
        print self.data

        MyTCPHandler.runningIpAddress = self.client_address[0]
        while self.data != 'QUIT':
            self.data = self.request.recv(1024).strip()
            if self.data == '':
                time.sleep(1)
            else:
                self.request.send

        MyTCPHandler.runningIpAddress = None
        subprocess.Popen(['pkill', 'tcpproxy_server'])

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

if __name__ == "__main__":
    HOST, PORT = "0.0.0.0", 6592
    server = ThreadedTCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()